package model;

import java.util.ArrayList;
import java.util.List;

public class ShapesWrapper {

	private List<Position> wrapper = new ArrayList<Position>();
	
	public ShapesWrapper() {
	}
	
	public void add(Position shape) {
		this.wrapper.add(shape);
	}
	
	public List<Position> getAll() {
		return this.wrapper;
	}
}
