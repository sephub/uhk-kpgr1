package model;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import gui.Canvas;
import gui.MainWindow;

public abstract class AbstractListener implements ActionListener, MouseListener, MouseMotionListener, MouseWheelListener, KeyListener {

	private Canvas taskPane;
	private MainWindow mainWindow;
	private Point point;
	
	/**
	 * 
	 * @param taskPane
	 * @param panel
	 * @param mainWindow
	 */
	public AbstractListener(Canvas taskPane, MainWindow mainWindow) {
		setTaskPane(taskPane);
		setMainWindow(mainWindow);
	}

	/**
	 * 
	 * @param taskPane
	 */
	public AbstractListener(Canvas taskPane) {
		this(taskPane, null);
	}
	
	/**
	 * @return the taskPane
	 */
	protected Canvas getTaskPane() {
		return taskPane;
	}

	/**
	 * @param taskPane the taskPane to set
	 */
	protected void setTaskPane(Canvas taskPane) {
		this.taskPane = taskPane;
	}

	/**
	 * @return the mainWindow
	 */
	protected MainWindow getMainWindow() {
		return mainWindow;
	}

	/**
	 * @param mainWindow the mainWindow to set
	 */
	protected void setMainWindow(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}
	
	/**
	 * @return the point
	 */
	protected Point getPoint() {
		return point;
	}

	/**
	 * @param point the point to set
	 */
	protected void setPoint(Point point) {
		this.point = point;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(java.awt.event.MouseEvent e) {
		getMainWindow().getStatusBar().getBar().setText( 
				String.format("X:%s; Y:%s", e.getX(), e.getY())
			);

		Color getColor = new Color(getTaskPane().getImage().getRGB(e.getX(), e.getY()));
		getMainWindow().getStatusBar().getColor().setBackground( getColor );
		getMainWindow().getStatusBar().getColor().setForeground( Color.WHITE);
		getMainWindow().getStatusBar().getColor().setText(
				String.format("R:%s, G:%s, B:%s", getColor.getRed(), getColor.getGreen(), getColor.getBlue())
			);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
