package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import shapes.Line;

public class PolygonShape implements Position {
	
	public final static int EPSILON = 1;
	
	private List<Line> shapes = new ArrayList<Line>();
	private Boolean filled = false;

	public PolygonShape(List<Line> shapes) {
		
		for (Line shape : shapes) {
			getShapes().add(shape);
		}

	}

	@Override
	public boolean hasPoints(Point p) {
		
		int intersects = 0;

		for ( Line line : getShapes()) {
			
			line.replaceTops();
			if (line.isIntersections(p)) {
				intersects++;
			}
		}

		return (intersects%2 == 0) ? false : true;
	}

	/**
	 * @return the shapes
	 */
	public List<Line> getShapes() {
		return (List<Line>) shapes;
	}

	/**
	 * @param shapes the shapes to set
	 */
	public void setShapes(List<Line> shapes) {
		this.shapes = shapes;
	}


	@Override
	public void setFilled(Boolean f) {
		filled = f;
	}


	@Override
	public Boolean isFilled() {
		return filled;
	}
	

}
