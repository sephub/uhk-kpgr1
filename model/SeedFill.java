package model;

import java.awt.Color;
import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import gui.Canvas;
import kpgrf1.Constans;
import shapes.Circle;
import shapes.Pixel;

public class SeedFill extends Thread {

	private Point point;
	private int color;
	private Canvas canvas;
	
	public SeedFill(List<Circle> shapes, int color) {
		super();
		setPoint(shapes.get(0).getStart());
		setColor(color);
	}

	/*
	 * 
	 * @param int color
	 */
	protected void setColor(int color) {
		this.color = color;
	}

	/**
	 * 
	 * @return int
	 */
	protected int getColor() {
		return this.color;
	}
	/**
	 * 
	 * @param taskPane
	 */
	public void run () {

			List<Point> founds = new LinkedList<Point>();
			
			boolean[][] s = new boolean[(int) (Constans.WINDOW_SIZE.getWidth())][(int) (Constans.WINDOW_SIZE.getHeight())];

			this.seed(s, founds ,getPoint().x, getPoint().y, new Color(Constans.ACTIVE_COLOR));
			
			for (Point point : founds) {
				Pixel.draw(canvas, point.x, point.y, Constans.FILLED_COLOR);
			}
	}

	public void seed(boolean[][] searched, List<Point> founds, int x, int y, Color borderColor) {
		
		if (x < 0 || y < 0 || x >= getCanvas().getImage().getWidth() || y >= getCanvas().getImage().getHeight()) {
			return;
		}
		
		Color background;

		try {
			background = new Color( getCanvas().getImage().getRGB(x, y) );	
		} catch (StackOverflowError e) {
			e.getStackTrace();
			return;
		}
		
		Point point  = new Point(x, y);

		try {
			if (searched[x][y]) {
				return;
			} else {
				searched[x][y] = true;

				if (background.equals(borderColor)) {
					return;
				} else {
					founds.add(point);
				}

				this.seed(searched, founds, x+1, y, borderColor);
				this.seed(searched, founds, x-1, y, borderColor);
				this.seed(searched, founds, x, y+1, borderColor);
				this.seed(searched, founds, x, y-1, borderColor);
			}
			
		} catch (StackOverflowError e) {
			e.getStackTrace();
			return;
		} catch (ArrayIndexOutOfBoundsException e) {
			e.getStackTrace();
			return;
		}
	}

	/**
	 * @return the point
	 */
	public Point getPoint() {
		return point;
	}

	/**
	 * @param point the point to set
	 */
	public void setPoint(Point point) {
		this.point = point;
	}

	/**
	 * @return the canvas
	 */
	public Canvas getCanvas() {
		return canvas;
	}

	/**
	 * @param canvas the canvas to set
	 */
	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

}
