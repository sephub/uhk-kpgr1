package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import gui.Canvas;
import kpgrf1.Constans;
import shapes.Line;

/**
 * implemetation of scan-line algorithm
 * @author sep
 *
 */
public class ScanLine {

	private List<Line> polygon;
	private List<Line> tmpPolygon = new ArrayList<Line>();
	private List<Integer> intersections = new ArrayList<Integer>();
	private Integer ymin = null, ymax = null;
	private Color color;

	public ScanLine(List<Line> shapes, int color) {
		this(shapes, new Color(color));
	}

	public ScanLine(List<Line> shapes, Color color) {
		setPolygon(shapes);
		setColor(color);
	}
	
	/**
	 * 
	 * @param canvas
	 */
	public void draw(Canvas canvas) {

		setYmin(canvas.getHeight());
		setYmax(0);
		
		for (Line line : getPolygon()) {

			if ( !line.isVertical()) {

				Line newLine = new Line(line.getX1(), line.getY1(), line.getX2(), line.getY2(), Constans.FILLED_COLOR);

				//change line orientation
				newLine.replaceTops();
				
				//compute k,q
				newLine.compute();
				
				// add to tmp
				getTmpPolygon().add(newLine);
				
				// get Y limits 
				if (getYmin() > newLine.getY2()) 
					setYmin(newLine.getY2());

				if (getYmax() < newLine.getY1())
					setYmax(newLine.getY1());
			}
		}
		
		for (int y = getYmin().intValue(); y < getYmax().intValue(); y++) {
			getIntersections().clear();
			
			for (Line tmpLine : getTmpPolygon()) {
				
				if (tmpLine.isIntersections(y)) {
					Integer ins = tmpLine.intersections(y);
					if (ins.intValue() > 0)
						getIntersections().add( ins );
				}
			}

			// sort intersections
			Collections.sort(getIntersections());

			// write line between intersections
			if (!getIntersections().isEmpty()) {
				
				for (int i = 0; i < getIntersections().size(); i+=2) {
					Line.draw(canvas, getIntersections().get(i), y, getIntersections().get((i + 1) % getIntersections().size()), y, getColor().getRGB());
				}
				
				for (Line line : getPolygon()) {
					line.setColor(Constans.FILLED_COLOR);
					line.draw(canvas);
				}
			}
			
		}
		
	}
	
	/**
	 * @return the polygon
	 */
	public List<Line> getPolygon() {
		return polygon;
	}

	/**
	 * @param polygon the polygon to set
	 */
	public void setPolygon(List<Line> polygon) {
		this.polygon = polygon;
	}

	/**
	 * @return the tmpPolygon
	 */
	public List<Line> getTmpPolygon() {
		return tmpPolygon;
	}

	/**
	 * @param tmpPolygon the tmpPolygon to set
	 */
	public void setTmpPolygon(List<Line> tmpPolygon) {
		this.tmpPolygon = tmpPolygon;
	}

	/**
	 * @return the ymin
	 */
	public Integer getYmin() {
		return ymin;
	}

	/**
	 * @param ymin the ymin to set
	 */
	public void setYmin(Integer ymin) {
		this.ymin = ymin;
	}

	/**
	 * @return the ymax
	 */
	public Integer getYmax() {
		return ymax;
	}

	/**
	 * @param ymax the ymax to set
	 */
	public void setYmax(Integer ymax) {
		this.ymax = ymax;
	}

	/**
	 * @return the intersections
	 */
	public List<Integer> getIntersections() {
		return intersections;
	}

	/**
	 * @param intersections the intersections to set
	 */
	public void setIntersections(List<Integer> intersections) {
		this.intersections = intersections;
	}

	/**
	 * 
	 * @return
	 */
	public Color getColor() {
		return this.color;
	}
	
	/**
	 * 
	 * @param color
	 */
	public Color setColor(Color color) {
		return this.color = color;
	}

}
