package model;

import java.util.List;

import gui.Canvas;
import kpgrf1.Constans;
import shapes.Line;
import transforms.Camera;
import transforms.Kubika;
import transforms.Mat4;
import transforms.Mat4Identity;
import transforms.Point3D;
import transforms.Vec3D;


public class GeometricRenderer implements Verticies, Edges {
	
	private List<Point3D> verticies;
	private List<Integer> edges;
	
	private Mat4 transMat = new Mat4Identity();
	private Mat4 mat = new Mat4Identity();

	private Canvas canvas;
	
	private Camera camera = null;
	private Mat4 viewMatrix = null;
	private Kubika curve = null;
	
	public GeometricRenderer(Canvas canvas) {
		this(canvas, null, null); 
	}

	public GeometricRenderer(Canvas canvas, List<Point3D> verticies, List<Integer> edges) {
		setVerticies(verticies);
		setEdges(edges);
		setCanvas(canvas); 
	}
	
	
	public void setMat(Mat4 mat) {
		this.mat = mat;
	}
	
	public Mat4 getMat() {
		return this.mat;
	}
	
	/**
	 * 
	 * @param color
	 */
	public void render(Integer color) {
		
		// prepare final matrix for count
		Mat4 matrix = getMat().mul( 
						getCamera().getViewMatrix().mul(getViewMatrix())
		);

		for (int i = 0; i < getEdges().size() - 1; i += 2) {
			int e1 = getEdges().get(i);
			int e2 = getEdges().get(i + 1);
			Point3D v1 = getVerticies().get(e1);
			Point3D v2 = getVerticies().get(e2);
			renderLine(matrix, v1, v2, color, Line.SOLID);
		}
		Point3D cross = new Point3D(0,0,0);
		Point3D x = new Point3D(0,2,0);
		Point3D y = new Point3D(2,0,0);
		Point3D z = new Point3D(0,0,2);

		// axis x-y-z
		renderLine(getCamera().getViewMatrix().mul(getViewMatrix()), cross, x, Constans.FILLED_COLOR, Line.DOTTED);
		renderLine(getCamera().getViewMatrix().mul(getViewMatrix()), cross, y, Constans.FILLED_COLOR, Line.DOTTED);
		renderLine(getCamera().getViewMatrix().mul(getViewMatrix()), cross, z, Constans.FILLED_COLOR, Line.DOTTED);

	}
	
	private void renderLine(Mat4 matrix, Point3D v1, Point3D v2, Integer color, String type) {
	
		Point3D v1t = v1.mul(matrix);
		Point3D v2t = v2.mul(matrix);
		if (v1t.w <= 0 || v2t.w <= 0) {
			return;
		}
		Vec3D v1d = v1t.dehomog();
		Vec3D v2d = v2t.dehomog();

		int x1, x2, y1, y2, width, height;
		width = getCanvas().getWidth();
		height = getCanvas().getHeight();

		x1 = (int) ((v1d.x  + 1) * 0.5 * (width - 1)); 
		y1 = (int) ((-v1d.y  + 1) * 0.5 * (height - 1));
		x2 = (int) ((v2d.x  + 1) * 0.5 * (width - 1)); 
		y2 = (int) ((-v2d.y  + 1) * 0.5 * (height - 1));
		
		Line.draw(getCanvas(), x1, y1, x2, y2, color, type, Line.BRESENHAM);
	}
	
	/**
	 * @return the canvas
	 */
	public Canvas getCanvas() {
		return canvas;
	}
	/**
	 * @param canvas the canvas to set
	 */
	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	@Override
	public List<Point3D> getVerticies() {
		return verticies;
	}

	@Override
	public List<Integer> getEdges() {
		return edges;
	}

	@Override
	public void setVerticies(List<Point3D> verticies) {
		this.verticies = verticies;
	}

	@Override
	public void setEdges(List<Integer> edges) {
		this.edges = edges;
	}

	/**
	 * @return the transMat
	 */
	public Mat4 getTransMat() {
		return transMat;
	}

	/**
	 * @param transMat the transMat to set
	 */
	public void setTransMat(Mat4 transMat) {
		this.transMat = transMat;
	}

	/**
	 * @return the camera
	 */
	public Camera getCamera() {
		return camera;
	}

	/**
	 * @param camera the camera to set
	 */
	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	/**
	 * @return the viewMatrix
	 */
	public Mat4 getViewMatrix() {
		return viewMatrix;
	}

	/**
	 * @param viewMatrix the viewMatrix to set
	 */
	public void setViewMatrix(Mat4 viewMatrix) {
		this.viewMatrix = viewMatrix;
	}

	/**
	 * @return the curve
	 */
	public Kubika getCurve() {
		return curve;
	}

	/**
	 * @param curve the curve to set
	 */
	public void setCurve(Kubika curve) {
		this.curve = curve;
	}
}
