package model;

import java.util.ArrayList;
import java.util.List;

import gui.Canvas;
import kpgrf1.Constans;
import shapes.Line;
import transforms.Camera;
import transforms.Kubika;
import transforms.Mat4;
import transforms.Mat4Identity;
import transforms.Point3D;
import transforms.Vec3D;


public class CurveRenderer {
	
	private List<Point3D> verticies = new ArrayList<Point3D>();
	
	private Mat4 transMat = new Mat4Identity();
	private Mat4 mat = new Mat4Identity();

	private Canvas canvas;
	
	private Camera camera = null;
	private Mat4 viewMatrix = null;
	private Kubika curve = null;
	
	public CurveRenderer(Canvas canvas) {
		this(canvas, null, null); 
	}

	public CurveRenderer(Canvas canvas, Kubika curve, List<Point3D> verticies) {
		setCanvas(canvas);
	}
	
	
	public void setMat(Mat4 mat) {
		this.mat = mat;
	}
	
	public Mat4 getMat() {
		return this.mat;
	}
	
	/**
	 * 
	 * @param color
	 */
	public void render(Integer color) {
		
		// prepare final matrix for count
		Mat4 matrix = getMat().mul( 
						getCamera().getViewMatrix().mul(getViewMatrix())
		);

		// TODO: refactor
		curve.init(getVerticies().get(0), getVerticies().get(1), getVerticies().get(2), getVerticies().get(3));
		
		Point3D startPoint = getVerticies().get(0).mul( matrix );
		
		for (float t=0; t <=1; t = t + 0.05f) {
			Point3D compute = curve.compute(t);

			Point3D computePoint = compute.mul( matrix );
			
			Vec3D p1d = startPoint.dehomog();
			Vec3D p2d = computePoint.dehomog();

			int x1, x2, y1, y2, width, height;
			width = getCanvas().getWidth();
			height = getCanvas().getHeight();

			x1 = (int) ((p1d.x  + 1) * 0.5 * (width - 1)); 
			y1 = (int) ((-p1d.y  + 1) * 0.5 * (height - 1));
			x2 = (int) ((p2d.x  + 1) * 0.5 * (width - 1)); 
			y2 = (int) ((-p2d.y  + 1) * 0.5 * (height - 1));

			Line.draw(getCanvas(), x1, y1, x2, y2, Constans.DRAGGED_COLOR, Line.SOLID, Line.BRESENHAM);

			startPoint = computePoint;
		}		
	}
	
	/**
	 * @return the canvas
	 */
	public Canvas getCanvas() {
		return canvas;
	}
	/**
	 * @param canvas the canvas to set
	 */
	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	/**
	 * @return the transMat
	 */
	public Mat4 getTransMat() {
		return transMat;
	}

	/**
	 * @param transMat the transMat to set
	 */
	public void setTransMat(Mat4 transMat) {
		this.transMat = transMat;
	}

	/**
	 * @return the camera
	 */
	public Camera getCamera() {
		return camera;
	}

	/**
	 * @param camera the camera to set
	 */
	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	/**
	 * @return the viewMatrix
	 */
	public Mat4 getViewMatrix() {
		return viewMatrix;
	}

	/**
	 * @param viewMatrix the viewMatrix to set
	 */
	public void setViewMatrix(Mat4 viewMatrix) {
		this.viewMatrix = viewMatrix;
	}

	/**
	 * @return the curve
	 */
	public Kubika getCurve() {
		return curve;
	}

	/**
	 * @param curve the curve to set
	 */
	public void setCurve(Kubika curve) {
		this.curve = curve;
	}

	/**
	 * @return the verticies
	 */
	public List<Point3D> getVerticies() {
		return verticies;
	}

	/**
	 * @param verticies the verticies to set
	 */
	public void setVerticies(List<Point3D> verticies) {
		this.verticies = verticies;
	}
}
