package model;

import java.util.List;

public interface Edges {

	/**
	 * 
	 * @return
	 */
	public List<Integer> getEdges();
	
	/**
	 * 
	 * @param edges
	 */
	public void setEdges(List<Integer> edges);
}
