package model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import shapes.Circle;

public class CircleShape implements Position {

	private List<Circle> shapes = new ArrayList<Circle>();
	private Boolean filled = false;
	
	/**
	 * 
	 * @param shape
	 */
	public CircleShape(Circle shape) {
		getShapes().add(shape);
	}
	
	@Override
	public boolean hasPoints(Point p) {
		int r = getShapes().get(0).getR();
		int r2 = Circle.getR(getShapes().get(0).getStart(), p);
		return r2 > r ? false : true;
	}

	@Override
	public void setFilled(Boolean f) {
		this.filled = f;
	}

	@Override
	public Boolean isFilled() {
		return this.filled;
	}

	@Override
	public List<Circle> getShapes() {
		return shapes;
	}

}
