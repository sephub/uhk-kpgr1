package model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.MainWindow;
import kpgrf1.Constans;
import tools.HtmlContentReader;
import gui.InfoDialog;

public class MenuListener implements ActionListener {

	private MainWindow mainWindow;

	public MenuListener(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	
		switch (e.getActionCommand()) {
		case Constans.MENU_FILE_EXIT:
			System.exit(0);
			break;
		case Constans.MENU_HELP_INFO:

			new InfoDialog(
					mainWindow, 
					Constans.DIALOG_HOW_TO_USE, 
					HtmlContentReader.readFile( getClass().getResourceAsStream(Constans.DIALOG_HOW_TO_USE_FILE) )
				);

			break;

		default:
			break;
		}
	}

}
