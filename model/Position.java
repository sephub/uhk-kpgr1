package model;

import java.awt.Point;
import java.util.List;

public interface Position {
	
	/**
	 * 
	 * @param p
	 * @return
	 */
	public boolean hasPoints(Point p);
	
	/**
	 * 
	 * @param f
	 */
	public void setFilled(Boolean f);
	
	/**
	 * 
	 * @return
	 */
	public Boolean isFilled();
	
	/**
	 * 
	 * @return
	 */
	public List<?> getShapes();

}
