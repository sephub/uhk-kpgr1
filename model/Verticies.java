package model;

import java.util.List;

import transforms.Point3D;

public interface Verticies {

	/**
	 * 
	 * @return
	 */
	public List<Point3D> getVerticies();
	

	/**
	 * 
	 * @param verticies
	 */
	public void setVerticies(List<Point3D> verticies);
}
