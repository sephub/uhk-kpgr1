package taskThree;

import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import kpgrf1.Constans;

@SuppressWarnings("serial")
public class ActionPanel extends JPanel {
	
	private JButton clearButton;
	private ButtonGroup group;
	private JRadioButton btnCube;
	private JRadioButton btnBlock;
	private JRadioButton btnPyramid;
	private JRadioButton btnRotateAction;
	private JRadioButton btnMoveAction;
	private ButtonGroup group2;
	private JRadioButton btnFerguson;
	private JRadioButton btnBezier;
	private JRadioButton btnCoons;
	private ButtonGroup group3;
	private JCheckBox chkCamera;

	public ActionPanel(EventsListener listener){
		
		listener.setPanel(this);
		
		//setLayout(new FlowLayout(FlowLayout.LEFT));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		
		/* ROW 1 */
		// Radia
		setBtnCube(new JRadioButton(Constans.LABEL_TASK3_CUBE));
		setBtnBlock(new JRadioButton(Constans.LABEL_TASK3_BLOCK));
		setBtnPyramid(new JRadioButton(Constans.LABEL_TASK3_PYRAMID));
		
		setBtnFerguson(new JRadioButton(Constans.LABEL_TASK3_CURVES_FERGUSON));
		setBtnBezier(new JRadioButton(Constans.LABEL_TASK3_CURVES_BEZZIER));
		setBtnCoons(new JRadioButton(Constans.LABEL_TASK3_CURVES_COONS));
		
		setClearButton(new JButton(Constans.BUTTON_TASK1_CLEAR));
		
		getBtnCube().setActionCommand(Constans.LABEL_TASK3_CUBE);
		getBtnBlock().setActionCommand(Constans.LABEL_TASK3_BLOCK);
		getBtnPyramid().setActionCommand(Constans.LABEL_TASK3_PYRAMID);
		getBtnFerguson().setActionCommand(Constans.LABEL_TASK3_CURVES_FERGUSON);
		getBtnBezier().setActionCommand(Constans.LABEL_TASK3_CURVES_BEZZIER);
		getBtnCoons().setActionCommand(Constans.LABEL_TASK3_CURVES_COONS);

		getClearButton().addActionListener(listener);
		getBtnCube().addActionListener(listener);
		getBtnBlock().addActionListener(listener);
		getBtnPyramid().addActionListener(listener);
		getBtnFerguson().addActionListener(listener);
		getBtnBezier().addActionListener(listener);
		getBtnCoons().addActionListener(listener);

		group = new ButtonGroup();
		group.add(getBtnCube());
		group.add(getBtnBlock());
		group.add(getBtnPyramid());
		
		group2 = new ButtonGroup();
		group2.add(getBtnFerguson());
		group2.add(getBtnBezier());
		group2.add(getBtnCoons());

		/* ROW 2 */
		setBtnRotateAction(new JRadioButton(Constans.LABEL_TASK3_ROTATE_ACTION));
		setBtnMoveAction(new JRadioButton(Constans.LABEL_TASK3_MOVE_ACTION));
		setChkCamera(new JCheckBox(Constans.LABEL_TASK3_SHOW_CAMERA));
		
		getBtnRotateAction().setActionCommand(Constans.LABEL_TASK3_ROTATE_ACTION);
		getBtnMoveAction().setActionCommand(Constans.LABEL_TASK3_MOVE_ACTION);
		getChkCamera().setActionCommand(Constans.LABEL_TASK3_SHOW_CAMERA);
		
		getBtnRotateAction().addActionListener(listener);
		getBtnMoveAction().addActionListener(listener);
		getChkCamera().addActionListener(listener);
		
		getBtnRotateAction().setSelected(true);
		
		group3 = new ButtonGroup();
		group3.add(getBtnRotateAction());
		group3.add(getBtnMoveAction());

		
		JPanel row1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel row2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
				
		row1.add(new JLabel(Constans.LABEL_TASK3_SHAPES));
		row1.add(getBtnCube());
		row1.add(getBtnBlock());
		row1.add(getBtnPyramid());
		
		row1.add(new JLabel(Constans.LABEL_TASK3_CURVES));
		row1.add(getBtnFerguson());
		row1.add(getBtnBezier());
		row1.add(getBtnCoons());

		row1.add(getClearButton());
		
		row2.add(new JLabel(Constans.LABEL_TASK3_CURRENT_ACTION));
		row2.add(getBtnRotateAction());
		row2.add(getBtnMoveAction());
		row2.add(chkCamera);
		
		add(row1);
		add(row2);

	}	

	/**
	 * @return the clearButton
	 */
	public JButton getClearButton() {
		return clearButton;
	}

	/**
	 * @param clearButton the clearButton to set
	 */
	public void setClearButton(JButton clearButton) {
		this.clearButton = clearButton;
	}

	/**
	 * @return the btnCube
	 */
	public JRadioButton getBtnCube() {
		return btnCube;
	}

	/**
	 * @param btnCube the btnCube to set
	 */
	public void setBtnCube(JRadioButton btnCube) {
		this.btnCube = btnCube;
	}

	/**
	 * @return the btnBlock
	 */
	public JRadioButton getBtnBlock() {
		return btnBlock;
	}

	/**
	 * @param btnBlock the btnBlock to set
	 */
	public void setBtnBlock(JRadioButton btnBlock) {
		this.btnBlock = btnBlock;
	}

	/**
	 * @return the btnPyramid
	 */
	public JRadioButton getBtnPyramid() {
		return btnPyramid;
	}

	/**
	 * @param btnPyramid the btnPyramid to set
	 */
	public void setBtnPyramid(JRadioButton btnPyramid) {
		this.btnPyramid = btnPyramid;
	}
	
	/**
	 * 
	 * @return
	 */
	public ButtonGroup getGroup() {
		return group;
	}
	
	/**
	 * 
	 * @param group
	 */
	public void setGroup(ButtonGroup group) {
		this.group = group;
	}
	/**
	 * 
	 * @return
	 */
	public ButtonGroup getGroup2() {
		return group2;
	}
	
	/**
	 * 
	 * @param group
	 */
	public void setGroup2(ButtonGroup group) {
		this.group2 = group;
	}
	/**
	 * 
	 * @return
	 */
	public ButtonGroup getGroup3() {
		return group3;
	}

	/**
	 * 
	 * @param group
	 */
	public void setGroup3(ButtonGroup group) {
		this.group3 = group;
	}

	/**
	 * @return the rotateAction
	 */
	public JRadioButton getBtnRotateAction() {
		return btnRotateAction;
	}

	/**
	 * @param rotateAction the rotateAction to set
	 */
	public void setBtnRotateAction(JRadioButton rotateAction) {
		this.btnRotateAction = rotateAction;
	}

	/**
	 * @return the moveAction
	 */
	public JRadioButton getBtnMoveAction() {
		return btnMoveAction;
	}

	/**
	 * @param moveAction the moveAction to set
	 */
	public void setBtnMoveAction(JRadioButton moveAction) {
		this.btnMoveAction = moveAction;
	}

	/**
	 * @return the btnFerguson
	 */
	public JRadioButton getBtnFerguson() {
		return btnFerguson;
	}

	/**
	 * @param btnFerguson the btnFerguson to set
	 */
	public void setBtnFerguson(JRadioButton btnFerguson) {
		this.btnFerguson = btnFerguson;
	}

	/**
	 * @return the btnBezier
	 */
	public JRadioButton getBtnBezier() {
		return btnBezier;
	}

	/**
	 * @param btnBezier the btnBezier to set
	 */
	public void setBtnBezier(JRadioButton btnBezier) {
		this.btnBezier = btnBezier;
	}

	/**
	 * @return the btnCoons
	 */
	public JRadioButton getBtnCoons() {
		return btnCoons;
	}

	/**
	 * @param btnCoons the btnCoons to set
	 */
	public void setBtnCoons(JRadioButton btnCoons) {
		this.btnCoons = btnCoons;
	}

	/**
	 * @return the chkCamera
	 */
	public JCheckBox getChkCamera() {
		return chkCamera;
	}

	/**
	 * @param chkCamera the chkCamera to set
	 */
	public void setChkCamera(JCheckBox chkCamera) {
		this.chkCamera = chkCamera;
	}
	
}
