package taskThree;

import java.awt.BorderLayout;
import javax.swing.JPanel;

import gui.Canvas;
import gui.MainWindow;
import kpgrf1.Constans;
import taskThree.ActionPanel;
import taskThree.EventsListener;

@SuppressWarnings("serial")
public class TaskPane extends JPanel{

	private Canvas cns;
	private ActionPanel panel;
	private MainWindow mainForm;
	
	public JPanel info;

	public TaskPane(MainWindow mainWindow) {

		setMainForm(mainWindow);
		setLayout(new BorderLayout());
		
		setCns( new Canvas( 
				(int) (Constans.WINDOW_SIZE.getWidth()),
				(int) (Constans.WINDOW_SIZE.getHeight())
			)
		);
	
		EventsListener eventListener = new EventsListener( getCns(), getMainForm() );
		setPanel( new ActionPanel(eventListener) );

		getCns().addMouseListener(eventListener);
		getCns().addMouseMotionListener(eventListener);
		getCns().addMouseWheelListener(eventListener);
		
		add(getPanel(), BorderLayout.NORTH);
		add(getCns(), BorderLayout.CENTER);

	}

	/**
	 * @return the mainForm
	 */
	public MainWindow getMainForm() {
		return this.mainForm;
	}

	/**
	 * @param mainForm the mainForm to set
	 */
	public void setMainForm(MainWindow mainForm) {
		this.mainForm = mainForm;
	}
	
	/**
	 * @return the cns
	 */
	public Canvas getCns() {
		return cns;
	}

	/**
	 * @param cns the cns to set
	 */
	public void setCns(Canvas cns) {
		this.cns = cns;
	}

	/**
	 * @return the panel
	 */
	public ActionPanel getPanel() {
		return panel;
	}

	/**
	 * @param panel the panel to set
	 */
	public void setPanel(ActionPanel panel) {
		this.panel = panel;
	}
}
