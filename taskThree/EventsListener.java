package taskThree;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;

import gui.CameraDialog;
import gui.Canvas;
import gui.MainWindow;
import kpgrf1.Constans;
import model.AbstractListener;
import model.CurveRenderer;
import model.GeometricRenderer;
import shapes.Block;
import shapes.Cube;
import shapes.GeometricShapesAbstract;
import shapes.Pyramid;
import transforms.Camera;
import transforms.Kubika;
import transforms.Mat4;
import transforms.Mat4Identity;
import transforms.Mat4PerspRH;
import transforms.Mat4RotX;
import transforms.Mat4RotY;
import transforms.Mat4RotZ;
import transforms.Mat4Scale;
import transforms.Mat4Transl;
import transforms.Point3D;
import transforms.Vec3D;

public class EventsListener extends AbstractListener {
	
	public final static String AXIS_X = "x"; 
	public final static String AXIS_Y = "y"; 
	public final static String AXIS_Z = "z"; 
	
	private ActionPanel panel;
	private GeometricRenderer renderer;
	private Camera camera = new Camera();
	private float camera_azimuth = 1.5f;
	private float camera_zenith = 3.0f;
	private Mat4 persp;
	private Mat4 transMat = new Mat4Identity();
	private CameraDialog dialog;
	private CurveRenderer rendereCurve;
	
	private GeometricShapesAbstract demoElement;
	private Kubika demoCurve;
	
	public EventsListener(Canvas taskPane, MainWindow mainWindow) {
		super(taskPane, mainWindow);

		persp = new Mat4PerspRH(Math.PI / 3, (float) getTaskPane().getHeight()/ getTaskPane().getWidth(), 0.1f, 100.0f);

		getCamera().setPosition(new Vec3D(3, 2, 2));
		getCamera().setAzimuth(Math.PI + Math.atan(1.0 / getCamera_azimuth()));
		getCamera().setZenith(-Math.atan(1.0 / getCamera_zenith()));
		
		renderer = new GeometricRenderer(taskPane);
		renderer.setCamera(getCamera());
		renderer.setViewMatrix(persp);
		
		rendereCurve = new CurveRenderer(getTaskPane());
		rendereCurve.setCamera(getCamera());
		rendereCurve.setViewMatrix(persp);		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		switch ( e.getActionCommand() ) {
			case Constans.LABEL_TASK3_CUBE:
			case Constans.LABEL_TASK3_BLOCK:
			case Constans.LABEL_TASK3_PYRAMID:

				setPoint(null);
				demoElement = null;
				transMat = new Mat4Identity();
				getTaskPane().clear();

				paintBaseShape(e.getActionCommand(), Constans.ACTIVE_COLOR);

				// curve have been chosen
				if (getPanel().getGroup2().getSelection() != null) {

					paintBaseCurve(getPanel().getGroup2().getSelection().getActionCommand(), Constans.DRAGGED_COLOR);						
				}

				getTaskPane().present();
				break;
			case Constans.LABEL_TASK3_CURVES_FERGUSON:
			case Constans.LABEL_TASK3_CURVES_BEZZIER:
			case Constans.LABEL_TASK3_CURVES_COONS:
				
				// paint only if block main shape has been selected
				if (demoElement == null) return;
				getTaskPane().clear();

				demoCurve = null;
				
				// paint
				paintBaseShape(e.getActionCommand(), Constans.ACTIVE_COLOR);
				paintBaseCurve(e.getActionCommand(), Constans.DRAGGED_COLOR);
				
				getTaskPane().present();				
				
				break;
			case Constans.BUTTON_TASK1_CLEAR:
				getPanel().getGroup().clearSelection();
				getPanel().getGroup2().clearSelection();
				demoElement = null;
				demoCurve = null;
				setPoint(null);
				renderer.setMat(transMat = new Mat4Identity());
				rendereCurve.setMat(transMat = new Mat4Identity());
				getTaskPane().clear();
				getTaskPane().present();
				break;
			case Constans.LABEL_TASK3_SHOW_CAMERA:
				
				if (getDialog() == null)
					setDialog(new CameraDialog(getMainWindow(), this));
				
				JCheckBox element = (JCheckBox) e.getSource();
				if (element.isSelected()) {
					getDialog().setVisible(true);
				} else {
					getDialog().setVisible(false);
				}
				break;
			case Constans.LABEL_TASK3_CAMERA_UP:
			case Constans.LABEL_TASK3_CAMERA_DOWN:
			case Constans.LABEL_TASK3_CAMERA_LEFT:
			case Constans.LABEL_TASK3_CAMERA_RIGHT:
				moveCameraAxis( e.getActionCommand() );
				break;
		}
	}


	@Override
	public void keyPressed(KeyEvent e) {

		switch (e.getKeyCode()) {
			case 37:
				moveCameraAxis( Constans.LABEL_TASK3_CAMERA_LEFT );
				break;
			case 38:
				moveCameraAxis( Constans.LABEL_TASK3_CAMERA_UP );
				break;
			case 39:
				moveCameraAxis( Constans.LABEL_TASK3_CAMERA_RIGHT );
				break;
			case 40:
				moveCameraAxis( Constans.LABEL_TASK3_CAMERA_DOWN );
				break;
		}

	}
	
	@Override
	public void mousePressed(java.awt.event.MouseEvent e) {

		setPoint(e.getPoint());
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent e) {

		setPoint(null);
	}
	
	
	@Override
	public void mouseDragged(java.awt.event.MouseEvent e) {

		if (demoElement == null) return;
		
		getTaskPane().clear();
		
		Mat4 action = new Mat4Identity();
		float f;

		switch ( getPanel().getGroup3().getSelection().getActionCommand() ) {
		
			case Constans.LABEL_TASK3_ROTATE_ACTION:

				switch (e.getModifiers()) {
					// ctrl + left button
					case 18:
						f = getRotateAngle(e.getPoint(), getPoint(), EventsListener.AXIS_Y);
						action = new Mat4RotX(f);
						break;
					// ctrl + shift + left button
					case 19:
						f = getRotateAngle(e.getPoint(), getPoint(), EventsListener.AXIS_Z);
						action = new Mat4RotZ(f);
						break;
					// only left button
					default:
						f = getRotateAngle(e.getPoint(), getPoint(), EventsListener.AXIS_X);
						action = new Mat4RotY(f);		
				}
				break;
			case Constans.LABEL_TASK3_MOVE_ACTION:
				
				switch (e.getModifiers()) {
					case 18:
						f = getMoveShift(e.getPoint(), getPoint(), EventsListener.AXIS_Y);
						action = new Mat4Transl(0, f, 0);
						break;
					case 19:
						f = getMoveShift(e.getPoint(), getPoint(), EventsListener.AXIS_Z);
						action = new Mat4Transl(0, 0, f);
						break;
					default:
						f = getMoveShift(e.getPoint(), getPoint(), EventsListener.AXIS_X);
						action = new Mat4Transl(f, 0, 0);		
				}
				break;
		}
		
		transMat = transMat.mul(action);
				
		renderer.setMat( transMat );
		renderer.render( Constans.ACTIVE_COLOR );

		if (demoCurve != null) {
			rendereCurve.setMat( transMat );
			rendereCurve.render(Constans.DRAGGED_COLOR);			
		}
		
		getTaskPane().present();
		setPoint(e.getPoint());

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {

		if (demoElement == null) return;
		
		getTaskPane().clear();

		float zoom = (float) Math.abs(e.getWheelRotation() - 0.1f);
		transMat = transMat.mul(
				new Mat4Scale(zoom, zoom, zoom)
		);

		renderer.setMat(transMat);
		renderer.render( Constans.ACTIVE_COLOR );

		if (demoCurve != null) {
			rendereCurve.setMat( transMat );
			rendereCurve.render(Constans.DRAGGED_COLOR);			
		}
		
		getTaskPane().present();	
	}
	
	/**
	 * 
	 * @param panel
	 */
	public void setPanel(ActionPanel panel) {
		this.panel = panel;
	}
	
	/**
	 * 
	 * @return
	 */
	public ActionPanel getPanel() {
		return this.panel;
	}

	/**
	 * @return the dialog
	 */
	public CameraDialog getDialog() {
		return dialog;
	}

	/**
	 * @param dialog the dialog to set
	 */
	public void setDialog(CameraDialog dialog) {
		this.dialog = dialog;
	}

	/**
	 * @return Camera
	 */
	public Camera getCamera() {
		return camera;
	}

	/**
	 * @return the camera_azimuth
	 */
	public float getCamera_azimuth() {
		return camera_azimuth;
	}

	/**
	 * @param camera_azimuth the camera_azimuth to set
	 */
	public void setCamera_azimuth(float camera_azimuth) {
		this.camera_azimuth = camera_azimuth;
	}

	/**
	 * @return the camera_zenith
	 */
	public float getCamera_zenith() {
		return camera_zenith > 0.1f ? camera_zenith : 0.2f;
	}

	/**
	 * @param camera_zenith the camera_zenith to set
	 */
	public void setCamera_zenith(float camera_zenith) {
		this.camera_zenith = camera_zenith;
	}

	
	private void paintBaseShape (String type, Integer color) {
		
		if (type == Constans.LABEL_TASK3_CUBE)
			demoElement = Cube.getDemo();
		else if (type == Constans.LABEL_TASK3_BLOCK)
			demoElement = Block.getDemo();
		else if (type == Constans.LABEL_TASK3_PYRAMID)
			demoElement = Pyramid.getDemo();
		
		if (demoElement == null) return; 

		renderer.setEdges(demoElement.getEdges());
		renderer.setVerticies(demoElement.getVerticies());
		
		if (transMat !=null) {
			renderer.setMat(transMat);
		}
		renderer.render(color);
	}
	
	/**
	 * 
	 * @param type
	 * @param color
	 */
	private void paintBaseCurve (String type, Integer color) {

		List<Point3D> tmp = new ArrayList<Point3D>();
		
		if (type == Constans.LABEL_TASK3_CURVES_FERGUSON) {
			
			demoCurve  = new Kubika(1);
			tmp.add(new Point3D(0,0,0));
			tmp.add(new Point3D(1,1,1));
			tmp.add(new Point3D(0,1,0));
			tmp.add(new Point3D(1,1,1));
		}
		else if (type == Constans.LABEL_TASK3_CURVES_BEZZIER) {
			
			demoCurve  = new Kubika(0);
			tmp.add(new Point3D(0,0,0));
			tmp.add(new Point3D(1,0,1));
			tmp.add(new Point3D(1,1,0));
			tmp.add(new Point3D(1,1,1));
		}
		else if (type == Constans.LABEL_TASK3_CURVES_COONS) {
			demoCurve  = new Kubika(2);
			tmp.add(new Point3D(0,1,0));
			tmp.add(new Point3D(1,1,0));
			tmp.add(new Point3D(0,0,1));
			tmp.add(new Point3D(1,1,1));
		}

		
		if (demoCurve == null) return;
		
		rendereCurve.getVerticies().addAll(tmp);
		rendereCurve.setCurve( demoCurve );

		if (transMat !=null) {
			rendereCurve.setMat(transMat);
		}
		rendereCurve.render(color);
	}
	
	/**
	 * 
	 * @param type
	 */
	private void moveCameraAxis ( String type ) {
		
		if (demoElement == null) return;
		
		switch ( type ) {

			case Constans.LABEL_TASK3_CAMERA_UP:
			case Constans.LABEL_TASK3_CAMERA_DOWN:
				if ( type == Constans.LABEL_TASK3_CAMERA_UP )
					setCamera_zenith( getCamera_zenith() + 0.2f );
				else
					setCamera_zenith( getCamera_zenith() - 0.2f );
				
				getCamera().setZenith( -Math.atan(1.0 / getCamera_zenith()) );

				break;
			case Constans.LABEL_TASK3_CAMERA_LEFT:
			case Constans.LABEL_TASK3_CAMERA_RIGHT:

				if ( type == Constans.LABEL_TASK3_CAMERA_LEFT)
					setCamera_azimuth( getCamera_azimuth() - 0.2f );
				else
					setCamera_azimuth( getCamera_azimuth() + 0.2f );

				getCamera().setAzimuth( Math.PI + Math.atan(2.0 / getCamera_azimuth()));

				break;
		}
		
		getTaskPane().clear();
		
		renderer.render(Constans.ACTIVE_COLOR);
		if(demoCurve != null) rendereCurve.render(Constans.DRAGGED_COLOR);
		
		getTaskPane().present();
	}

	/**
	 * 
	 * @param currentPoint
	 * @param previousPoint
	 * @param axis
	 * @return
	 */
	float getRotateAngle (Point currentPoint, Point previousPoint, String axis) {
		
		float f = 0;
		
		switch (axis) {
		case EventsListener.AXIS_X:
			
			if( currentPoint.y > previousPoint.y ) f = (float) Math.PI/36;
			else f = -(float) Math.PI/36;				
			break;
		default:
			
			if( currentPoint.x > previousPoint.x ) f = (float) Math.PI/36;
			else f = -(float) Math.PI/36;				
		}
		
		return f;
	}

	/**
	 * 
	 * @param currentPoint
	 * @param previousPoint
	 * @param axis
	 * @return
	 */
	float getMoveShift (Point currentPoint, Point previousPoint, String axis) {
		
		float f = 0;
		
		switch (axis) {
			case EventsListener.AXIS_Z:

				if(currentPoint.y > previousPoint.y) f = -0.1f;
				else f = 0.1f;
				break;
			default:
				
				if(currentPoint.x > previousPoint.x) f = 0.1f;
				else f = -0.1f;
		}

		return f;
	}
	
}
