package kpgrf1;

import java.awt.Dimension;

public final class Constans {

	public final static String WINDOW_TITLE = "Pavel Severýn : KPGRF1";
	public final static Dimension WINDOW_SIZE = new Dimension(800, 600);
	public final static Dimension CANVAS_SIZE = new Dimension(800, 600);
	public final static Dimension DIALOG_SIZE = new Dimension(500, 400);
	
	public final static String TAB_TITLE_TASK1 = "Task one";
	public final static String TAB_TITLE_TASK2 = "Task two";
	public final static String TAB_TITLE_TASK3 = "Task three";
	public final static int ACTIVE_COLOR = 0xFF00FF;
	public final static int FILLED_COLOR = 0x00AA00;
	public final static int DRAGGED_COLOR = 0x0000DD;
	
	public final static String LABEL_TASK1_TRIVIAL = "line - trivial";
	public final static String LABEL_TASK1_BRESENHAN = "line - bresenham";
	public final static String LABEL_TASK1_CIRCLE = "circle";
	
	public final static String BUTTON_TASK1_CLEAR = "clear";
	
	public final static String LABEL_TASK2_POLYGON = "polygon";
	public final static String LABEL_TASK2_CIRCLE = "circle";
	public final static String BUTTON_TASK2_FILL = "fill";
	
	public final static String LABEL_TASK3_SHAPES = "Shapes:";
	public final static String LABEL_TASK3_CURRENT_ACTION = "Current action:";
	public final static String LABEL_TASK3_CURVES = "Curves:";
	
	public final static String LABEL_TASK3_CUBE = "cube";
	public final static String LABEL_TASK3_BLOCK = "block";
	public final static String LABEL_TASK3_PYRAMID = "pyramid";
	
	public final static String LABEL_TASK3_CURVES_FERGUSON = "Ferguson";
	public final static String LABEL_TASK3_CURVES_BEZZIER = "Bezier";
	public final static String LABEL_TASK3_CURVES_COONS = "Coons";
	
	public final static String LABEL_TASK3_MOVE_ACTION = "move";
	public final static String LABEL_TASK3_ROTATE_ACTION = "rotate";
	
	public final static String LABEL_TASK3_SHOW_CAMERA = "Show camera control";
	public final static String LABEL_TASK3_CAMERA_UP = "up";
	public final static String LABEL_TASK3_CAMERA_DOWN = "down";
	public final static String LABEL_TASK3_CAMERA_LEFT = "left";
	public final static String LABEL_TASK3_CAMERA_RIGHT = "right";
	
	public final static String COMBO_TASK3_ISOM = "isometric";
	public final static String COMBO_TASK3_PERSP = "perspektiv";
	
	public final static String DIALOG_TASK3_TITLE = "Camera control panel";
	
	public final static String MENU_FILE = "File";
	public final static String MENU_FILE_EXIT = "Exit";
	public final static String MENU_HELP = "Help";
	public final static String MENU_HELP_INFO = "How use";
	
	public final static String DIALOG_HOW_TO_USE = "How use";

	public final static String DIALOG_HOW_TO_USE_FILE = "/files/how_to_use.html";	
}
