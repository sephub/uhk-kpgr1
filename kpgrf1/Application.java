package kpgrf1;

import javax.swing.SwingUtilities;

import gui.MainWindow;

public class Application {

	public Application() {
		MainWindow window = new MainWindow();
		window.setResizable(false);
		window.setVisible(true);
		window.pack();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Application();
			}
		});
	}

}
