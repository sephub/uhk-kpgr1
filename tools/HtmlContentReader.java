package tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HtmlContentReader {

	/**
	 * 
	 * @param inputStream
	 * @return
	 */
	public static String readFile(InputStream inputStream) {
		
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder contentBuilder = new StringBuilder();
        String str;

        try {
			while ((str = r.readLine()) != null) {
				contentBuilder.append(str);
			}
		} catch (IOException e) {
			return "";
		}
        
        try {
			r.close();
		} catch (IOException e) {
			return "";
		}
        
		return contentBuilder.toString();
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 */
	public static String readFile(String file) {
		
		StringBuilder contentBuilder = new StringBuilder();
		
		try {
		    BufferedReader in = new BufferedReader(new FileReader(file));
		    String str;
		    while ((str = in.readLine()) != null) {
		        contentBuilder.append(str);
		    }
		    in.close();
		} catch (IOException e) {
			return null;
		}
		
		String content = contentBuilder.toString();
		
		return content;
	}
}
