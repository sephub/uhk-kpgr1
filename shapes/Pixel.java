package shapes;

import java.awt.Color;
import java.awt.Point;

import gui.Canvas;

public class Pixel {

	private int x;
	private int y;
	private int color;

	/**
	 * 
	 * @param x
	 * @param y
	 * @param color
	 */
	public Pixel (int x, int y, int color) {
		setX(x);
		setY(y);
		setColor(color);
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param color
	 */
	public Pixel(int x, int y, Color color) {
		this(x, y, color.getRGB());
	}

	/**
	 * 
	 * @param point
	 * @param color
	 */
	public Pixel(Point point, Color color) {
		this((int)point.getX(), (int) point.getY(), color.getRGB());
	}

	/**
	 * 
	 * @param point
	 * @param color
	 */
	public Pixel(Point point, int color) {
		this((int)point.getX(), (int) point.getY(), color);
	}
	
	/**
	 * 
	 * @param canvas
	 */
	public void draw(Canvas canvas) {
		Pixel.draw(canvas, getX(), getY(), getColor());
	}
	
	/**
	 * 
	 * @param canvas
	 * @param x
	 * @param y
	 * @param color
	 */
	public static void draw(Canvas canvas, int x, int y, int color) {
		if (canvas != null) {
			try {
				canvas.getImage().setRGB(x, y, color);
			} catch (ArrayIndexOutOfBoundsException e) {
				e.getStackTrace();
			}
		}
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the color
	 */
	public int getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(int color) {
		this.color = color;
	}

}
