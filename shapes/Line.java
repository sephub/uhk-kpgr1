package shapes;

import gui.Canvas;

public class Line extends LineAbstract {

	public final static String TRIVIAL = "trivial";
	public final static String BRESENHAM = "bresenham";

	public final static String SOLID = "solid";
	public final static String DOTTED = "dotted";

	public Line( int x1, int y1, int x2, int y2, int color) {
		super(x1, y1, x2, y2, color);
	}

	public static void draw(Canvas canvas, int x1, int y1, int x2, int y2, int color, String type, String style) {
		new Line(x1,y1, x2, y2, color).draw(canvas, type, style);
	}	
	/**
	 * 
	 * @param canvas
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param color
	 * @param type
	 */
	public static void draw(Canvas canvas, int x1, int y1, int x2, int y2, int color, String type) {
		new Line(x1,y1, x2, y2, color).draw(canvas, type);
	}

	/**
	 * 
	 * @param canvas
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param color
	 */
	public static void draw(Canvas canvas, int x1, int y1, int x2, int y2, int color) {
		new Line(x1,y1, x2, y2, color).draw(canvas);
	}
	
	/**
	 * 
	 * @param canvas
	 * @param type
	 */
	public void draw (Canvas canvas, String type, String style) {
		
		if (type == Line.BRESENHAM) 
			this.drawBresenham(canvas, style);
		else
			this.drawTrivial(canvas, style);

	}

	public void draw (Canvas canvas, String type) {
		
		if (type == Line.BRESENHAM) 
			this.drawBresenham(canvas);
		else
			this.drawTrivial(canvas);

	}	

	/**
	 * 
	 * @param canvas
	 */
	public void draw(Canvas canvas) {
		this.drawTrivial(canvas);
	}

	/**
	 * 
	 * @param canvas
	 */
	private final void drawBresenham(Canvas canvas) {
		drawBresenham(canvas, Line.SOLID);
	}

	/**
	 * 
	 * @param canvas
	 */
	private final void drawBresenham(Canvas canvas, String style) {

		int dy, dx, x1, y1, x2, y2, p1, sy, sx, color;
		x1 = getX1();
		x2 = getX2();
		y1 = getY1();
		y2 = getY2();
		color = getColor();

		dx = Math.abs(x2 - x1);
	    dy = Math.abs(y2 - y1);
	    p1 = dx - dy;

	    if (x1 < x2) sx = 1; else sx = -1;
	    if (y1 < y2) sy = 1; else sy = -1;

	    Pixel.draw(canvas, x1, y1, color);
	    
	    while ((x1 != x2) || (y1 != y2)) {  

	        int p = 2 * p1;

	        if (p < dx) {
	            p1 = p1 + dx;
	            y1 = y1 + sy;
	        }

	        if (p > -dy) {
	            p1 = p1 - dy;
	            x1 = x1 + sx;
	        }
	        
			if (style == Line.DOTTED && x1 % 5 != 0)
				continue;
	        
	        Pixel.draw(canvas, x1, y1, color);
	    }		
	}

	/**
	 * 
	 * @param canvas
	 */
	private final void drawTrivial(Canvas canvas) {
		drawTrivial(canvas, Line.SOLID);
	}
	
	/**
	 * 
	 * @param canvas
	 */
	private final void drawTrivial(Canvas canvas, String style) {
		
		int dy, dx, x1, y1, x2, y2, color;
		x1 = getX1();
		x2 = getX2();
		y1 = getY1();
		y2 = getY2();
		color = getColor();
		
		dy = y2-y1;
		dx = x2-x1;
	
		if ( Math.abs(dx) > Math.abs(dy) ) {
			double k = (double)dy/(double)dx;
			double q = y1 - k * x1;
			
			if(dx < 0)
				dx = -1;
			else
				dx = 1;

			while(x1!=x2) {
				x1 += dx;
				if (style == Line.DOTTED && x1 % 5 != 0)
					continue;
				
				y1 = (int) Math.round( k*x1 + q );
				Pixel.draw(canvas, x1, y1, color);
			}

		} else {
			double k = (double)dx/(double)dy;
			double q = x1 - k * y1;
			
			if( dy < 0 )
				dy = -1;
			else
				dy = 1;
			
			while(y1!=y2) {
				y1 += dy;
				if (style == Line.DOTTED && y1 % 5 != 0)
					continue;
					
				x1 = (int) Math.round(k*y1+q);
				Pixel.draw(canvas, x1, y1, color);
			}			
		}		
	}
	
	
}
