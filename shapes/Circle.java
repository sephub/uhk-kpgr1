package shapes;

import java.awt.Color;
import java.awt.Point;

import gui.Canvas;

public class Circle {

	private Point start;
	private Point end;
	private int color;
	private int r;

	/**
	 * 
	 * @param start
	 * @param color
	 */
	public Circle(Point start, Point end, int color) {
		setStart(start);
		setEnd(end);
		setColor(color);
		setR(start, end);
	}

	public Circle(Point start, int r, int color) {
		this(start,null, color);
		setR(r);
	}
	
	/**
	 * 
	 * @param canvas
	 * @param point
	 * @param color
	 */
	public static void draw(Canvas canvas, Point point1, Point point2, Color color){
		draw( canvas, point1, point2, color.getRGB() );
	}
	
	/**
	 * 
	 * @param canvas
	 * @param point
	 * @param r
	 * @param color
	 */
	public static void draw(Canvas canvas, Point point, int r, Color color){
		new Circle(point, r, color.getRGB()).draw(canvas);
	}	

	/**
	 * 
	 * @param canvas
	 * @param point
	 * @param r
	 * @param color
	 */
	public static void draw(Canvas canvas, Point point, int r, int color){
		new Circle(point, r, color).draw(canvas);
	}	

	/**
	 * 
	 * @param canvas
	 * @param point1
	 * @param point2
	 * @param color
	 */
	public static void draw(Canvas canvas, Point point1, Point point2, int color){
		new Circle(point1, point2, color).draw(canvas);
	}
		
	/**
	 * 
	 * @param canvas
	 */
	public void draw(Canvas canvas){

		int X2, Y2, p, X, Y, x, y;
		r = getR();
		x = getStart().x;
		y = getStart().y;

		
		X2 = 3;
		Y2 = 2*r - 2;
		p = 1 - r;
		X = 0;
		Y = r;
		
		while (X <= Y) {
			
			Pixel.draw(canvas, x + X, y + Y, color); //[+x,+y]
			Pixel.draw(canvas, x + Y, y - X, color); //[+y,-x]
			Pixel.draw(canvas, x - X, y - Y, color); //[-x,-y]
			Pixel.draw(canvas, x - Y, y + X, color); //[-y,+x]
			
			Pixel.draw(canvas, x + Y, y + X, color); //[+y,+x]
			Pixel.draw(canvas, x + X, y - Y, color); //[+x,-y]
			Pixel.draw(canvas, x - Y, y - X, color); //[-y,-x]
			Pixel.draw(canvas, x - X, y + Y, color); //[-x,+y]
				
			if (p > 0) {
				p = p - Y2;
				Y2 = Y2 - 2;
				Y = Y - 1;
			}
		
			p = p + X2;
			X2 += 2;
			X += 1;
		}
		
		
		
	}

	/**
	 * @return the point
	 */
	public Point getStart() {
		return start;
	}

	/**
	 * @param point the point to set
	 */
	public void setStart (Point point) {
		this.start = point;
	}

	/**
	 * @return the color
	 */
	public int getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(int color) {
		this.color = color;
	}

	/**
	 * @return the end
	 */
	public Point getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(Point end) {
		this.end = end;
	}
	
	/**
	 * 
	 * @param r
	 */
	public void setR(int r) {
		this.r = r;
	}
	
	/**
	 * 
	 * @param start
	 * @param end
	 */
	public void setR(Point start, Point end) {
		
		if (start != null && end != null) {						
			setR( getR(start, end) );
		}

	}
	
	public static int getR(Point start, Point end) {
		
		int r = (int) Math.sqrt( 
				Math.pow(end.getX()-start.getX(), 2) + Math.pow(end.getY()-start.getY(), 2) 
			);
		return r;
		
	}
	
	/**
	 * 
	 * @return
	 */
	public int getR() {
		return this.r;
	}

	public String toString() {
		return String.format(
				"CIRCLE => X1:%s, Y1:%s, X2:%s, Y2:%s, R:%s, COLOR:%s\n", 
				this.getStart().x, this.getStart().y, this.getEnd().x, this.getEnd().y, this.getR(), this.getColor()
				);
	}
	
}
