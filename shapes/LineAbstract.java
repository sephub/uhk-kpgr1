package shapes;

import java.awt.Point;

public abstract class LineAbstract {
	
		private int x1;
		private int y1;
		private int x2;
		private int y2;
		private int color;
		
		private double q;
		private double k;
		
		/**
		 * 
		 * @param x1
		 * @param y1
		 * @param x2
		 * @param y2
		 * @param color
		 */
		public LineAbstract( int x1, int y1, int x2, int y2, int color) {
			setX1(x1);
			setX2(x2);
			setY1(y1);
			setY2(y2);
			setColor(color);
		}
		/**
		 * 
		 * @param p1
		 * @param p2
		 * @param color
		 */
		public LineAbstract(Point p1, Point p2, int color) {
			setX1(p1.x);
			setX2(p2.x);
			setY1(p1.y);
			setY2(p2.y);
			setColor(color);
		}

		/**
		 * 
		 * @return
		 */
		public boolean isVertical() {
			
			if (getY1() == getY2()) return true;
			else return false;
		}
		
		/**
		 * @return 
		 * 
		 */
		public void replaceTops(){
			
			if (this.getY1() < this.getY2()) {
				int x1, x2, y1, y2;
				x1 = getX1();
				x2 = getX2();
				y1 = getY1();
				y2 = getY2();

				setX1(x2);setY1(y2);setX2(x1);setY2(y1);
			}
		}
		
		//  y=k*x + q
		public void compute() {
			
			// k = (y2-y1) / (x2 - 1)
			setK( (double)( getX2() - getX1() ) / ( getY2() - getY1() ) );
			// q = x1 - k * y1
			setQ( getX1() - (getK() * getY1()) );
		}

		/**
		 * (y >= y2) && (y < y1)
		 * @param y
		 * @return
		 */
		public boolean isIntersections(int y) {
			
			if ( y > getY2() && y <= getY1() ) return true; 
			
			return false;
		}

		/**
		 * 
		 * @param p
		 * @return
		 */
		public boolean isIntersections(Point p) {
			
			if ( p.x < Math.min(getX1(), getX2()) ||
				 p.y >= getY1() || p.y < getY2()
				) return false;
			
			if ( p.x > Math.max(getX1(), getX2())) {
				return true;
			} else {

				float k_line = 0, k_point = 0;

				// check wheater is line
				if ( Math.abs(getX1() - getX2() ) > 0) {
					//for base line 
					k_line = ( (float) (getY2() - getY1()) / (float) (getX2() - getX1()));
				}
				
				// check wheater is line
				if ( Math.abs(getX1() - p.x) > 0 ) {
					// for line from start to checked point
					k_point = ( (float) (p.y - getY1()) / (float) (p.x - getX1()) );
				}

				return (boolean) (k_point >= k_line);
			}
		}
		
		/**
		 * x = k*y + q 
		 * @param y
		 * @return
		 */
		public Integer intersections (int y) {
				
			// x = k*y + q
			return new Integer( (int) (( getK() * (double) y) + getQ()) );
		}
		
		/**
		 * @return the x1
		 */
		public int getX1() {
			return x1;
		}

		/**
		 * @param x1 the x1 to set
		 */
		public void setX1(int x1) {
			this.x1 = x1;
		}

		/**
		 * @return the y1
		 */
		public int getY1() {
			return y1;
		}

		/**
		 * @param y1 the y1 to set
		 */
		public void setY1(int y1) {
			this.y1 = y1;
		}

		/**
		 * @return the x2
		 */
		public int getX2() {
			return x2;
		}

		/**
		 * @param x2 the x2 to set
		 */
		public void setX2(int x2) {
			this.x2 = x2;
		}

		/**
		 * @return the y2
		 */
		public int getY2() {
			return y2;
		}

		/**
		 * @param y2 the y2 to set
		 */
		public void setY2(int y2) {
			this.y2 = y2;
		}

		/**
		 * @return the color
		 */
		public int getColor() {
			return color;
		}

		/**
		 * @param color the color to set
		 */
		public void setColor(int color) {
			this.color = color;
		}

		/**
		 * 
		 */
		public String toString() {
			return String.format(
					"X1:%s, Y1:%s, X2:%s, Y2:%s, COLOR:%s\n", 
					this.getX1(), this.getY1(), this.getX2(), this.getY2(), this.getColor()
					);
		}
		
		/**
		 * @return the k
		 */
		public double getK() {
			return k;
		}
		
		/**
		 * @param k the k to set
		 */
		public void setK(double k) {
			this.k = k;
		}
		
		/**
		 * @return the q
		 */
		public double getQ() {
			return q;
		}
		
		/**
		 * @param q the q to set
		 */
		public void setQ(double q) {
			this.q = q;
		}
		
		public Point getPointA () {
			return new Point(getX1(), getY1());
		}
		
		public Point getPointB () {
			return new Point(getX2(), getY2());
		}		
}
