package shapes;

import java.util.ArrayList;
import java.util.List;

import model.Edges;
import model.Verticies;
import transforms.Point3D;

public abstract class GeometricShapesAbstract implements Verticies, Edges {

	protected List<Point3D> verticies = new ArrayList<Point3D>();
	protected List<Integer> edges = new ArrayList<Integer>();
	
	/**
	 * 
	 */
	@Override
	public List<Point3D> getVerticies() {
		return verticies;
	}

	/**
	 * 
	 */
	@Override
	public List<Integer> getEdges() {
		return edges;
	}

	@Override
	public void setVerticies(List<Point3D> verticies) {
		this.verticies = verticies;
		
	}

	@Override
	public void setEdges(List<Integer> edges) {
		this.edges = edges;
	}
}
