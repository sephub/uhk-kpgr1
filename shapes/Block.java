package shapes;

import java.util.ArrayList;
import java.util.List;

import transforms.Point3D;

public class Block extends GeometricShapesAbstract{

	/**
	 * 
	 * @param verticies
	 * @param edges
	 */
	public Block( List<Point3D> verticies, List<Integer> edges) {
		setVerticies(verticies);
		setEdges(edges);
	}

	/**
	 *  Demo cube 
	 * @return
	 */
	public static GeometricShapesAbstract getDemo() {

		List <Point3D> verticies = new ArrayList<Point3D>();
		verticies.add(new Point3D(0,1,0));
		verticies.add(new Point3D(1,1,0));
		verticies.add(new Point3D(1,0,0));
		verticies.add(new Point3D(0,0,0));
		verticies.add(new Point3D(0,1,2));
		verticies.add(new Point3D(1,1,2));
		verticies.add(new Point3D(1,0,2));
		verticies.add(new Point3D(0,0,2));
	
		List <Integer> edges = new ArrayList<Integer>();
		
		for (int i = 0; i < 4; i++) {
			edges.add(i);
			edges.add((i + 1) % 4);
			edges.add(i);
			edges.add(i + 4);
			edges.add(i + 4);
			edges.add((i + 1) % 4 + 4);
		}

		return new Block(verticies, edges);
	}

	/**
	 * 
	 */
	public String toString() {
		return String.format("V1: %s, V2: %s, V3: %s, V4: %s, V5: %s, V6: %s, V7: %s, V8: %s");
	}
}
