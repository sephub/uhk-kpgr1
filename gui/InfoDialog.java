package gui;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

import kpgrf1.Constans;

@SuppressWarnings("serial")
public class InfoDialog extends JDialog {
	
	public InfoDialog(MainWindow parent, String title, String message) {
		this(parent, title, message, "text/html");
	}
	
	public InfoDialog(MainWindow parent, String title, String message, String type) {
		super(parent, title, true);
		
		JEditorPane content = new JEditorPane(type, null);
		content.setText(message);
		content.setEditable(false);
		content.setVisible(true);
		
		JScrollPane contentScrol = new JScrollPane(content);
		getContentPane().add(contentScrol, BorderLayout.CENTER);
		
		setSize(Constans.DIALOG_SIZE);

		int x = parent.getLocation().x + ((parent.getWidth() - (int) getSize().getWidth()) / 2);  
		int y = parent.getLocation().y + ((parent.getHeight() - (int) getSize().getHeight()) / 2);

		setLocation(x,y);
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setVisible(true);
		pack();
	}
}
