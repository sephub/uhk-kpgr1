package gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;

import kpgrf1.Constans;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {

	
	private StatusBar statusBar;
	private Menu menu;

	public MainWindow() {
		super(Constans.WINDOW_TITLE);
		setLayout( new BorderLayout() );
		setSize(Constans.WINDOW_SIZE);
		 
		ContentPane tabbedPane = new ContentPane();
		tabbedPane.addTab(Constans.TAB_TITLE_TASK1, new taskOne.TaskPane(this));
		tabbedPane.addTab(Constans.TAB_TITLE_TASK2, new taskTwo.TaskPane(this));
		tabbedPane.addTab(Constans.TAB_TITLE_TASK3, new taskThree.TaskPane(this));

		tabbedPane.setSelectedIndex(2);
		
		statusBar = new StatusBar();
		add(tabbedPane, BorderLayout.CENTER);
		add(statusBar, BorderLayout.SOUTH);
		
		setMenu(new Menu(this));
		setJMenuBar(getMenu());

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void setStatusbar(StatusBar statusbar) {
		this.statusBar = statusbar;
	}
	
	public StatusBar getStatusBar() {
		return this.statusBar;
	}

	/**
	 * @return the menu
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

}