package gui;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import kpgrf1.Constans;
import model.MenuListener;

@SuppressWarnings("serial")
public class Menu extends JMenuBar{

	private JMenu menuFile;
	private JMenuItem menuFileExit;
	private JMenu menuHelp;
	private JMenuItem menuHelpInfo;

	public Menu(MainWindow mainWindow) {
		
		MenuListener listener = new MenuListener(mainWindow);
		
		setMenuFile(new JMenu(Constans.MENU_FILE));

		setMenuFileExit(new JMenuItem(Constans.MENU_FILE_EXIT));
		getMenuFileExit().setActionCommand(Constans.MENU_FILE_EXIT);
		getMenuFileExit().addActionListener(listener);

		getMenuFile().add(getMenuFileExit());

		setMenuHelp(new JMenu(Constans.MENU_HELP));
		setMenuHelpInfo(new JMenuItem(Constans.MENU_HELP_INFO));
		getMenuHelpInfo().setActionCommand(Constans.MENU_HELP_INFO);
		getMenuHelpInfo().addActionListener(listener);
		getMenuHelp().add(getMenuHelpInfo());
		
		add(getMenuFile());
		add(getMenuHelp());
	}

	/**
	 * @return the menuFile
	 */
	public JMenu getMenuFile() {
		return menuFile;
	}

	/**
	 * @param menuFile the menuFile to set
	 */
	public void setMenuFile(JMenu menuFile) {
		this.menuFile = menuFile;
	}

	/**
	 * @return the menuFileExit
	 */
	public JMenuItem getMenuFileExit() {
		return menuFileExit;
	}

	/**
	 * @param menuFileExit the menuFileExit to set
	 */
	public void setMenuFileExit(JMenuItem menuFileExit) {
		this.menuFileExit = menuFileExit;
	}

	/**
	 * @return the menuHelp
	 */
	public JMenu getMenuHelp() {
		return menuHelp;
	}

	/**
	 * @param menuHelp the menuHelp to set
	 */
	public void setMenuHelp(JMenu menuHelp) {
		this.menuHelp = menuHelp;
	}

	/**
	 * @return the menuHelpInfo
	 */
	public JMenuItem getMenuHelpInfo() {
		return menuHelpInfo;
	}

	/**
	 * @param menuHelpInfo the menuHelpInfo to set
	 */
	public void setMenuHelpInfo(JMenuItem menuHelpInfo) {
		this.menuHelpInfo = menuHelpInfo;
	}
}
