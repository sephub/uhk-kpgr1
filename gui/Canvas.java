package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Canvas extends JPanel {

	private BufferedImage image;
	
	public Canvas(int width, int height) {
		setPreferredSize(new Dimension(width, height));
		setBackground(new Color(0, 0, 0));		
		this.setImage(new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB));
	}

	/**
	 * 
	 */
	public void present() {
		if (getGraphics() != null){
			getGraphics().drawImage(getImage(), 0, 0, null);
		}
	}
	
	/**
	 * 
	 */
	public void clear() {
		Graphics gr = getImage().getGraphics();
		gr.setColor(new Color(0,0,0));
		gr.fillRect(0, 0, getImage().getWidth(), getImage().getHeight());
	}
	
	/**
	 * 
	 */
	public int getWidth() {
		return getImage().getWidth();
	}
	
	public int getHeight() {
		return getImage().getHeight();
	}	

	/**
	 * @return the image
	 */
	public BufferedImage getImage() {
		return image;
	}


	/**
	 * @param image the image to set
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
	}

}
