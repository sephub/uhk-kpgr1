package gui;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;

import kpgrf1.Constans;
import taskThree.EventsListener;

@SuppressWarnings("serial")
public class CameraDialog extends JDialog {
	
	private JButton btnUp;
	private JButton btnDown;
	private JButton btnLeft;
	private JButton btnRight;

	public CameraDialog(MainWindow parent, EventsListener listener) {
		
		setTitle(Constans.DIALOG_TASK3_TITLE);
		
		setBtnUp(new JButton( Constans.LABEL_TASK3_CAMERA_UP ));
		setBtnDown(new JButton( Constans.LABEL_TASK3_CAMERA_DOWN ));
		setBtnLeft(new JButton( Constans.LABEL_TASK3_CAMERA_LEFT ));
		setBtnRight(new JButton( Constans.LABEL_TASK3_CAMERA_RIGHT ));

		getBtnUp().addActionListener(listener);
		getBtnDown().addActionListener(listener);
		getBtnLeft().addActionListener(listener);
		getBtnRight().addActionListener(listener);
		
		getBtnUp().addKeyListener(listener);
		getBtnDown().addKeyListener(listener);
		getBtnLeft().addKeyListener(listener);
		getBtnRight().addKeyListener(listener);
		
		add( getBtnUp(), BorderLayout.NORTH );
		add( getBtnDown(), BorderLayout.SOUTH );
		add( getBtnLeft(), BorderLayout.WEST );
		add( getBtnRight(), BorderLayout.EAST );
		
		int x = parent.getLocation().x + (parent.getWidth());  
		int y = parent.getLocation().y + ((parent.getHeight() - (int) getSize().getHeight()) / 5);

		setLocation(x,y);
		setResizable(false);
		setVisible(true);
		pack();
	}

	/**
	 * @return the btnUp
	 */
	public JButton getBtnUp() {
		return btnUp;
	}

	/**
	 * @param btnUp the btnUp to set
	 */
	public void setBtnUp(JButton btnUp) {
		this.btnUp = btnUp;
	}

	/**
	 * @return the btnDown
	 */
	public JButton getBtnDown() {
		return btnDown;
	}

	/**
	 * @param btnDown the btnDown to set
	 */
	public void setBtnDown(JButton btnDown) {
		this.btnDown = btnDown;
	}

	/**
	 * @return the btnLeft
	 */
	public JButton getBtnLeft() {
		return btnLeft;
	}

	/**
	 * @param btnLeft the btnLeft to set
	 */
	public void setBtnLeft(JButton btnLeft) {
		this.btnLeft = btnLeft;
	}

	/**
	 * @return the btnRight
	 */
	public JButton getBtnRight() {
		return btnRight;
	}

	/**
	 * @param btnRight the btnRight to set
	 */
	public void setBtnRight(JButton btnRight) {
		this.btnRight = btnRight;
	}
}
