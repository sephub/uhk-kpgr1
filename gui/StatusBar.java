package gui;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class StatusBar extends JPanel {

	private JLabel bar;
	private JLabel color;

	public StatusBar() {
		setLayout( new FlowLayout(FlowLayout.LEFT) );
		setBar(new JLabel());
		add(getBar());
		
		setColor(new JLabel("Color"));
		getColor().setOpaque(true);
		add(getColor());
	}

	/**
	 * @return the souradnice
	 */
	public JLabel getBar() {
		return bar;
	}

	/**
	 * @param souradnice the bar to set
	 */
	public void setBar(JLabel bar) {
		this.bar = bar;
	}

	/**
	 * @return the color
	 */
	public JLabel getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(JLabel color) {
		this.color = color;
	}
}
