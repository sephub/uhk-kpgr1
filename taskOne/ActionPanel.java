package taskOne;

import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import kpgrf1.Constans;

@SuppressWarnings("serial")
public class ActionPanel extends JPanel {
	
	private JRadioButton btnTrivial;
	private JRadioButton btnBresenham;
	private JRadioButton btnCircle;
	private JButton clearButton;
	private ButtonGroup group;

	public ActionPanel(EventsListener listener){
		
		listener.setPanel(this);

		setLayout(new FlowLayout(FlowLayout.LEFT));

		// Radia
		setBtnTrivial( new JRadioButton(Constans.LABEL_TASK1_TRIVIAL) );
		setBtnBresenham(new JRadioButton(Constans.LABEL_TASK1_BRESENHAN));
		setBtnCircle(new JRadioButton(Constans.LABEL_TASK1_CIRCLE));
		
		// Action button
		setClearButton(new JButton(Constans.BUTTON_TASK1_CLEAR));
		
		getBtnTrivial().setSelected(true);
		getBtnTrivial().setActionCommand(Constans.LABEL_TASK1_TRIVIAL);
		getBtnBresenham().setActionCommand(Constans.LABEL_TASK1_BRESENHAN);
		getBtnCircle().setActionCommand(Constans.LABEL_TASK1_CIRCLE);

		getClearButton().addActionListener(listener);
		
		group = new ButtonGroup();
		group.add(getBtnTrivial());
		group.add(getBtnCircle());
		group.add(getBtnBresenham());

		add(getBtnTrivial());
		add(getBtnBresenham());
		add(getBtnCircle());
		add(getClearButton());

	}	
	/**
	 * @return the btnTrivial
	 */
	public JRadioButton getBtnTrivial() {
		return btnTrivial;
	}

	/**
	 * @param btnTrivial the btnTrivial to set
	 */
	public void setBtnTrivial(JRadioButton btnTrivial) {
		this.btnTrivial = btnTrivial;
	}

	/**
	 * @return the clearButton
	 */
	public JButton getClearButton() {
		return clearButton;
	}

	/**
	 * @param clearButton the clearButton to set
	 */
	public void setClearButton(JButton clearButton) {
		this.clearButton = clearButton;
	}
	
	/**
	 * 
	 * @param group
	 */
	public void setRadioGroup(ButtonGroup group) {
		this.group = group;
	}
	
	/**
	 * 
	 * @return
	 */
	public ButtonGroup getRadioGroup() {
		return this.group;
	}
	/**
	 * @return the btnCircle
	 */
	public JRadioButton getBtnCircle() {
		return btnCircle;
	}
	/**
	 * @param btnCircle the btnCircle to set
	 */
	public void setBtnCircle(JRadioButton btnCircle) {
		this.btnCircle = btnCircle;
	}
	/**
	 * @return the btnBresenham
	 */
	public JRadioButton getBtnBresenham() {
		return btnBresenham;
	}
	/**
	 * @param btnBresenham the btnBresenham to set
	 */
	public void setBtnBresenham(JRadioButton btnBresenham) {
		this.btnBresenham = btnBresenham;
	}
}
