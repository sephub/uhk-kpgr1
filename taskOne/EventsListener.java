package taskOne;

import java.awt.Point;
import java.awt.event.ActionEvent;

import gui.Canvas;
import gui.MainWindow;
import kpgrf1.Constans;
import model.AbstractListener;
import shapes.Circle;
import shapes.Line;
import taskOne.ActionPanel;

public class EventsListener extends AbstractListener {

	private ActionPanel panel;

	public EventsListener(Canvas taskPane, MainWindow mainWindow) {
		super(taskPane, mainWindow);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		switch (e.getActionCommand()) {			
			case Constans.BUTTON_TASK1_CLEAR:
				getTaskPane().clear();
				getTaskPane().present();
				break;
			default:
				break;
		}
	}

	@Override
	public void mousePressed(java.awt.event.MouseEvent e) {
		setPoint(new Point(e.getX(), e.getY()));
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent e) {
		switch (getPanel().getRadioGroup().getSelection().getActionCommand()) {

			case Constans.LABEL_TASK1_TRIVIAL:
				Line.draw(getTaskPane(), getPoint().x, getPoint().y, e.getX(), e.getY(), Constans.ACTIVE_COLOR, Line.TRIVIAL);
				break;
			
			case Constans.LABEL_TASK1_BRESENHAN:
				Line.draw(getTaskPane(), getPoint().x, getPoint().y, e.getX(), e.getY(), Constans.ACTIVE_COLOR, Line.BRESENHAM);
				break;

			case Constans.LABEL_TASK1_CIRCLE:
				Circle.draw(getTaskPane(), getPoint(), new Point(e.getX(), e.getY()), Constans.ACTIVE_COLOR);
				break;
		}

		getTaskPane().present();
		
	}

	@Override
	public void mouseDragged(java.awt.event.MouseEvent e) {

			getTaskPane().clear();
		
			switch (getPanel().getRadioGroup().getSelection().getActionCommand()) {
	
				case Constans.LABEL_TASK1_TRIVIAL:
					Line.draw(getTaskPane(), getPoint().x, getPoint().y, e.getX(), e.getY(), Constans.DRAGGED_COLOR, Line.TRIVIAL);
					break;
				case Constans.LABEL_TASK1_BRESENHAN:
					Line.draw(getTaskPane(), getPoint().x, getPoint().y, e.getX(), e.getY(), Constans.DRAGGED_COLOR, Line.BRESENHAM);
					break;
				case Constans.LABEL_TASK1_CIRCLE:
					Circle.draw(getTaskPane(), getPoint(), new Point(e.getX(), e.getY()), Constans.DRAGGED_COLOR);
					break;
			}
		
		getTaskPane().present();

	}
	
	/**
	 * 
	 * @param panel
	 */
	public void setPanel(ActionPanel panel) {
		this.panel = panel;
	}
	
	/**
	 * 
	 * @return
	 */
	public ActionPanel getPanel() {
		return this.panel;
	}

}
