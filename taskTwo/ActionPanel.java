package taskTwo;

import java.awt.FlowLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;

import kpgrf1.Constans;

@SuppressWarnings("serial")
public class ActionPanel extends JPanel {
	
	private JRadioButton btnPolygon;
	private JRadioButton btnCirlce;
	private JToggleButton btnFill;
	private JButton clearButton;
	private ButtonGroup group;

	public ActionPanel(EventsListener listener){
		
		listener.setPanel(this);
		
		setLayout(new FlowLayout(FlowLayout.LEFT));

		// Radia
		setBtnPolygon( new JRadioButton(Constans.LABEL_TASK2_POLYGON) );
		setBtnCirlce(new JRadioButton(Constans.LABEL_TASK2_CIRCLE));
		
		// Action button
		setBtnFill(new JToggleButton(Constans.BUTTON_TASK2_FILL));

		setClearButton(new JButton(Constans.BUTTON_TASK1_CLEAR));
		
		getBtnPolygon().setSelected(true);
		getBtnPolygon().setActionCommand(Constans.LABEL_TASK2_POLYGON);
		getBtnCirlce().setActionCommand(Constans.LABEL_TASK2_CIRCLE);

		getBtnFill().addActionListener(listener);
		getClearButton().addActionListener(listener);
		
		group = new ButtonGroup();
		group.add(getBtnPolygon());
		group.add(getBtnCirlce());
		
		getBtnPolygon().addActionListener(listener);
		getBtnCirlce().addActionListener(listener);
		
		add(getBtnPolygon());
		add(getBtnCirlce());
		
		add(getBtnFill());
		add(getClearButton());

	}	

	/**
	 * @return the clearButton
	 */
	public JButton getClearButton() {
		return clearButton;
	}

	/**
	 * @param clearButton the clearButton to set
	 */
	public void setClearButton(JButton clearButton) {
		this.clearButton = clearButton;
	}
	
	/**
	 * 
	 * @param group
	 */
	public void setRadioGroup(ButtonGroup group) {
		this.group = group;
	}
	
	/**
	 * 
	 * @return
	 */
	public ButtonGroup getRadioGroup() {
		return this.group;
	}
	/**
	 * @return the btnPolygon
	 */
	public JRadioButton getBtnPolygon() {
		return btnPolygon;
	}
	/**
	 * @param btnPolygon the btnPolygon to set
	 */
	public void setBtnPolygon(JRadioButton btnPolygon) {
		this.btnPolygon = btnPolygon;
	}

	/**
	 * @return the btnFill
	 */
	public JToggleButton getBtnFill() {
		return btnFill;
	}

	/**
	 * @param btnFill the btnFill to set
	 */
	public void setBtnFill(JToggleButton btnFill) {
		this.btnFill = btnFill;
	}

	/**
	 * @return the btnCirlce
	 */
	public JRadioButton getBtnCirlce() {
		return btnCirlce;
	}

	/**
	 * @param btnCirlce the btnCirlce to set
	 */
	public void setBtnCirlce(JRadioButton btnCirlce) {
		this.btnCirlce = btnCirlce;
	}

}
