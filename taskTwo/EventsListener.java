package taskTwo;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import gui.Canvas;
import gui.MainWindow;
import kpgrf1.Constans;
import model.AbstractListener;
import model.CircleShape;
import model.PolygonShape;
import model.Position;
import model.ScanLine;
import model.SeedFill;
import model.ShapesWrapper;
import shapes.Circle;
import shapes.Line;

public class EventsListener extends AbstractListener {
	
	private ActionPanel panel;
	private Point polygonStart;

	private List<Line> shapesWrapper = new ArrayList<Line>() ;
	private ShapesWrapper shapes = new ShapesWrapper();

	public EventsListener(Canvas taskPane, MainWindow mainWindow) {
		super(taskPane, mainWindow);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		switch (e.getActionCommand()) {
			case Constans.LABEL_TASK2_CIRCLE:
			case Constans.LABEL_TASK2_POLYGON:				
				setPoint(null);
				setPolygonStart(null);
				getShapesWrapper().clear();
				getTaskPane().present();
				break;
			case Constans.BUTTON_TASK1_CLEAR:
				setPoint(null);
				getShapesWrapper().clear();
				getTaskPane().clear();
				getTaskPane().present();
				getShapes().getAll().clear();
				break;
			case Constans.BUTTON_TASK2_FILL:
				if (getPanel().getBtnFill().isSelected()) {
					
					getMainWindow().setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
					
					if ( !getShapesWrapper().isEmpty() ) {
						endPolygon( new Point(getPoint().x, getPoint().y), Constans.ACTIVE_COLOR);
						getTaskPane().present();
					}
					
				} else {
					
					getMainWindow().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}
				break;
			default:
				break;
		}
	}

	@Override
	public void mousePressed(java.awt.event.MouseEvent e) {

		if (!getPanel().getBtnFill().isSelected()) {

			if ( getPoint() == null && e.getButton() == MouseEvent.BUTTON1 ) {

				setPoint( new Point(e.getX(), e.getY()) );
				
				switch (getPanel().getRadioGroup().getSelection().getActionCommand()) {
					case Constans.LABEL_TASK2_POLYGON:
						if ( getPolygonStart() == null ) {
							setPolygonStart( new Point(e.getX(), e.getY()) );
						}
						break;
				}
			}
		}
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent e) {

		if (!getPanel().getBtnFill().isSelected()) {

			// repaint painted shapes
			repaintShapes();
			
			switch (getPanel().getRadioGroup().getSelection().getActionCommand()) {
	
				case Constans.LABEL_TASK2_POLYGON:
					
					if (e.getButton() == MouseEvent.BUTTON1) {
						Line tmp = new Line(getPoint().x, getPoint().y, e.getX(), e.getY(), Constans.ACTIVE_COLOR);
						tmp.draw(getTaskPane());

						if ( !getPoint().equals(e.getPoint()) ) {
							getShapesWrapper().add( tmp );
						}

						setPoint(new Point(e.getX(), e.getY()));
						
						// dotted line to start
						Line.draw(getTaskPane(), getPolygonStart().x, getPolygonStart().y, e.getX(), e.getY(), Constans.DRAGGED_COLOR, Line.TRIVIAL, Line.DOTTED);

					} else if (e.getButton() == MouseEvent.BUTTON3) {
						
						if ( !getShapesWrapper().isEmpty() ) {
							endPolygon( new Point(getPoint().x, getPoint().y), Constans.ACTIVE_COLOR);
						}
					}
					break;
				case Constans.LABEL_TASK2_CIRCLE:
					
					if (e.getButton() == MouseEvent.BUTTON1) {
						
						Circle circle = new Circle( getPoint(), e.getPoint(), Constans.ACTIVE_COLOR);
						
						circle.draw(getTaskPane());

						getShapes().add(new CircleShape(circle));
						
						setPoint( null );
						
					}
					break;
			}


			getTaskPane().present();
		}		
	}
	
	
	@Override
	public void mouseDragged(java.awt.event.MouseEvent e) {
		
		if (!getPanel().getBtnFill().isSelected()) {

			// repaint painted shapes
			repaintShapes();

			switch (getPanel().getRadioGroup().getSelection().getActionCommand()) {
	
				case Constans.LABEL_TASK2_POLYGON:
					Line.draw(getTaskPane(), getPoint().x, getPoint().y, e.getX(), e.getY(), Constans.DRAGGED_COLOR);
					// dotted line to start
					Line.draw(getTaskPane(), getPolygonStart().x, getPolygonStart().y, e.getX(), e.getY(), Constans.DRAGGED_COLOR, Line.TRIVIAL, Line.DOTTED);
					break;
				case Constans.LABEL_TASK2_CIRCLE:
					Circle circle = new Circle(getPoint(), new Point(e.getX(), e.getY()), Constans.DRAGGED_COLOR); 
					circle.draw(getTaskPane());
					break;
			}
			
			getTaskPane().present();
		}
	}

	@Override
	public void mouseClicked(java.awt.event.MouseEvent e) {

		if (getPanel().getBtnFill().isSelected()) {
			
			for (Position shape : getShapes().getAll() ) {

				if ( shape.hasPoints(e.getPoint()) ) {
					
					if (shape instanceof PolygonShape) {
						ScanLine s = new ScanLine( ((PolygonShape) shape).getShapes() , Constans.FILLED_COLOR );
						s.draw(getTaskPane());						
					} else if (shape instanceof CircleShape) {
						SeedFill s = new SeedFill( ( (CircleShape) shape).getShapes(), Constans.FILLED_COLOR); 
						s.setCanvas(getTaskPane());
						s.start();
						try {
							s.join();
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}
					
					shape.setFilled(true);
					getTaskPane().present();
					
				}							
			}
			
		}
	}

	/**
	 * 
	 * @param start
	 * @param color
	 */
	public void endPolygon(Point start, int color) {
		
		Line line = new Line(start.x, start.y, getPolygonStart().x, getPolygonStart().y, color);
		line.draw(getTaskPane());
		
		getShapesWrapper().add( line );
		
		getShapes().add(new PolygonShape(getShapesWrapper()));

		setPoint( null );
		setPolygonStart( null );
		getShapesWrapper().clear();
		
	}
	
	public void repaintShapes() {

		getTaskPane().clear();
		
		for (Position shape : getShapes().getAll()) {
			
			if (shape instanceof PolygonShape) {
				for (Line line : ((PolygonShape) shape).getShapes() ) {
					line.draw(getTaskPane());
				}

				if (shape.isFilled()) {
					ScanLine s = new ScanLine( ((PolygonShape) shape).getShapes() , Constans.FILLED_COLOR );
					s.draw(getTaskPane());
				}					
			} else if (shape instanceof CircleShape) {

				for (Circle circle : ((CircleShape) shape).getShapes() ) {
					circle.draw(getTaskPane());
				}					
				
				if (shape.isFilled()) {
					SeedFill s = new SeedFill( ((CircleShape) shape).getShapes() , Constans.FILLED_COLOR );
					s.setCanvas(getTaskPane());
					s.run();
				}					

			}
			
		}

		for (Line line : getShapesWrapper()) {
			line.draw(getTaskPane());
		}		
	}
	
	/**
	 * 
	 * @param panel
	 */
	public void setPanel(ActionPanel panel) {
		this.panel = panel;
	}
	
	/**
	 * 
	 * @return
	 */
	public ActionPanel getPanel() {
		return this.panel;
	}

	/**
	 * @return the shapesWrapper
	 */
	public List<Line> getShapesWrapper() {
		return (List<Line>) shapesWrapper;
	}

	/**
	 * @param shapesWrapper the shapesWrapper to set
	 */
	public void setShapesWrapper(List<Line> shapesWrapper) {
		this.shapesWrapper = shapesWrapper;
	}

	/**
	 * @return the polygonStart
	 */
	public Point getPolygonStart() {
		return polygonStart;
	}

	/**
	 * @param polygonStart the polygonStart to set
	 */
	public void setPolygonStart(Point polygonStart) {
		this.polygonStart = polygonStart;
	}

	/**
	 * @return the shapes
	 */
	public ShapesWrapper getShapes() {
		return shapes;
	}

	/**
	 * @param shapes the shapes to set
	 */
	public void setShapes(ShapesWrapper shapes) {
		this.shapes = shapes;
	}
}
